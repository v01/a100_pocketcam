﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using A100_PocketCam.Models;
using A100_PocketCam.Utils;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace A100_PocketCam.API
{
    static class APIAuth
    {
        public static string UserToken = null;
        public static User ActualUser = null;
        public static bool LoggedIn = false;
        public static bool HasCameraRequest = false;

        public static void SetUser(User user)
        {
            ActualUser = user;
        }

        public static async Task<System.Net.HttpStatusCode> GetToken(Context context)
        {
            if (UserToken == null)
            {
                try
                {
                    string json = JsonConvert.SerializeObject(ActualUser);
                    HttpContent content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                    HttpClient client = new HttpClient();
                    HttpRequestMessage request = new HttpRequestMessage();
                    request.RequestUri = new Uri("https://a100.technovik.ru:1000/api/auth/authenticate");
                    request.Method = HttpMethod.Post;
                    request.Content = content;
                    HttpResponseMessage response = await client.SendAsync(request);
                    var resp = await response.Content.ReadAsStringAsync();
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var map = JObject.Parse(resp);
                        string token = (string)map["acessToken"];
                        UserToken = token;
                        A100Toaster.Show(context, "Добро пожаловать, " + ActualUser.login + "!");
                        LoggedIn = true;
                        return response.StatusCode;
                    }
                    else
                    {
                        A100Toaster.Show(context, "Ошибка входа! Непрвильный логин или пароль!");
                        LoggedIn = false;
                        return System.Net.HttpStatusCode.Unauthorized;
                    }
                }
                catch (Exception e)
                {
                    A100Toaster.Show(context, e.Message);
                    LoggedIn = false;
                    return System.Net.HttpStatusCode.Unauthorized;
                }
            }
            return System.Net.HttpStatusCode.Unauthorized;
        }
        public static async void CheckToken()
        {
            // проверка токена на актуальность
        }
        public static async void CameraRequest(Activity activity, Context context)
        {
            
        }

    }
}