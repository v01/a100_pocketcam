﻿using A100_PocketCam.API;
using A100_PocketCam.Models;
using A100_PocketCam.Utils;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace A100_PocketCam
{
    [Activity(Label = "@string/app_title", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, BottomNavigationView.IOnNavigationItemSelectedListener
    {
        BottomNavigationView navigation;

        EditText etLogin;
        EditText etPassword;
        CheckBox boxSaveLoginData;
        Button btLogin;
        Button btUnLogin;

        ImageView ivCameraResult;
        Button btOpenCamera;
        Button btSendPhotoToServer;
        Button btSaveToGallery;

        TextView tvNotReady;
        string ApplicationPath;
        string fileName = "saved_user.json";

        byte[] BitmapData = null;

        bool IsNeedLoginSave = false;

        JObject Requests;



        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            this.RequestedOrientation = ScreenOrientation.Locked;

            Requests = null;

            ApplicationPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            // Инициализация компонентов вкладки входа
            etLogin = FindViewById<EditText>(Resource.Id.etLogin);
            etPassword = FindViewById<EditText>(Resource.Id.etPassword);
            boxSaveLoginData = FindViewById<CheckBox>(Resource.Id.boxSaveLoginData);
            btLogin = FindViewById<Button>(Resource.Id.btLogin);
            btUnLogin = FindViewById<Button>(Resource.Id.btUnLogin);
            btLogin.Click += BtLogin_Click;
            btUnLogin.Click += BtUnLogin_Click;
            boxSaveLoginData.CheckedChange += BoxSaveLoginData_CheckedChange;
            
            // Инициализация компонентов вкладки камеры
            ivCameraResult = FindViewById<ImageView>(Resource.Id.ivCameraResult);
            btOpenCamera = FindViewById<Button>(Resource.Id.btOpenCamera);
            btSendPhotoToServer = FindViewById<Button>(Resource.Id.btSendPhotoToServer);
            btSaveToGallery = FindViewById<Button>(Resource.Id.btSaveToGallery);
            btOpenCamera.Click += BtOpenCamera_Click;
            btSendPhotoToServer.Click += BtSendPhotoToServer_Click;

            // Инициализация компонентов вкладки галереи
            tvNotReady = FindViewById<TextView>(Resource.Id.tvNotReady);

            // скрытитие всех элементов кроме элементов входа
            SetCameraVisibility(ViewStates.Gone);
            SetGalleryVisibility(ViewStates.Gone);

            // var json = JsonConvert.SerializeObject(APIAuth.ActualUser);
            var filePath = ApplicationPath + @"\" + fileName;
            if (File.Exists(filePath))
            { 
                User user = null;
                using (StreamReader r = new StreamReader(filePath))
                {
                    string json = r.ReadToEnd();
                    user = JsonConvert.DeserializeObject<User>(json);
                }
                if (user != null)
                {
                    try
                    {
                        etLogin.Text = user.login;
                        etPassword.Text = user.password;
                        APIAuth.SetUser(new Models.User(etLogin.Text, etPassword.Text));
                        var a = await APIAuth.GetToken(ApplicationContext);
                        if (a == System.Net.HttpStatusCode.OK)
                        {
                            btLogin.Enabled = false;
                            etLogin.Enabled = false;
                            etPassword.Enabled = false;
                            boxSaveLoginData.Enabled = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        A100Toaster.Show(ApplicationContext, "Не удалось загрузить данные для входа.");
                        etLogin.Text = "";
                        etPassword.Text = "";
                    }
                }
            }


            navigation = FindViewById<BottomNavigationView>(Resource.Id.navigation);
            navigation.SetOnNavigationItemSelectedListener(this);


            // Блоировка всех компонентов кроме входа
            if (!APIAuth.LoggedIn)
            {
                btOpenCamera.Enabled = false;
                btSaveToGallery.Enabled = false;
                btSendPhotoToServer.Enabled = false;
            }


            
        }

        private void BtUnLogin_Click(object sender, EventArgs e)
        {
            etLogin.Text = "";
            etPassword.Text = "";
            etLogin.Enabled = true;
            etPassword.Enabled = true;
            boxSaveLoginData.Enabled = true;
            btLogin.Enabled = true;
            APIAuth.ActualUser = null;
            APIAuth.UserToken = null;
            APIAuth.LoggedIn = false;
            btUnLogin.Enabled = false;
        }

        public void SetLoginVisibility(ViewStates value)
        {
            etLogin.Visibility = value;
            etPassword.Visibility = value;
            boxSaveLoginData.Visibility = value;
            btLogin.Visibility = value;
            btUnLogin.Visibility = value;
        }
        public void SetCameraVisibility(ViewStates value)
        {
            ivCameraResult.Visibility = value;
            btOpenCamera.Visibility = value;
            btSendPhotoToServer.Visibility = value;
            btSaveToGallery.Visibility = value;
        }
        public void SetGalleryVisibility(ViewStates value)
        {
            tvNotReady.Visibility = value;
        }

        private void BtSendPhotoToServer_Click(object sender, EventArgs e)
        {
            if (BitmapData == null)
            {
                btSendPhotoToServer.Enabled = false;
                btSaveToGallery.Enabled = false;
                A100Toaster.Show(ApplicationContext, "Нечего отправлять!");
            }
            else
                UpdateRequest();
        }

        private async void UpdateRequest()
        {
            try
            {
                // A100Toaster.Show(ApplicationContext, Requests["cameraEventId"].ToString());
                // A100Toaster.Show(ApplicationContext, BitmapData[0].ToString());

                CameraEvents CameraEvent = new CameraEvents() { CameraEventId = (int)Requests["cameraEventId"], ImageByte = BitmapData };
                string json = JsonConvert.SerializeObject(CameraEvent);
                HttpContent content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                HttpClient client = new HttpClient();
                HttpRequestMessage request = new HttpRequestMessage();
                request.RequestUri = new Uri("https://a100.technovik.ru:1000/api/vik/UpdateCameraEvent");
                request.Method = HttpMethod.Post;
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", APIAuth.UserToken);
                request.Content = content;
                HttpResponseMessage response = await client.SendAsync(request);
                if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                {
                    A100Toaster.Show(ApplicationContext, "Нужно перезайти в приложение!");
                }
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    A100Toaster.Show(ApplicationContext, "Отправлена фотография для повреждения");
                }
                // A100Toaster.Show(ApplicationContext, response.StatusCode.ToString());
                // var str = await response.Content.ReadAsStringAsync();
                // A100Toaster.Show(ApplicationContext, str);
                // var map = JObject.Parse(str);
                BitmapData = null;
            }
            catch (Exception ex)
            {
                A100Toaster.Show(ApplicationContext, ex.Message);
            }
        }

        private void BoxSaveLoginData_CheckedChange(object sender, CompoundButton.CheckedChangeEventArgs e)
        {
            IsNeedLoginSave = e.IsChecked;
        }
        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            try
            {
                base.OnActivityResult(requestCode, resultCode, data);
                Bitmap bitmap = (Bitmap)data.Extras.Get("data");
                ivCameraResult.SetImageBitmap(bitmap);


                using (var stream = new MemoryStream())
                {
                    bitmap.Compress(Bitmap.CompressFormat.Png, 0, stream);
                    BitmapData = stream.ToArray();
                }
                btSaveToGallery.Enabled = true;
                btSendPhotoToServer.Enabled = true;
            }
            catch
            {
                btSaveToGallery.Enabled = false;
                btSendPhotoToServer.Enabled = false;
            }
        }

        private async void BtLogin_Click(object sender, System.EventArgs e)
        {
            APIAuth.SetUser(new Models.User(etLogin.Text, etPassword.Text));
            var a = await APIAuth.GetToken(ApplicationContext);
            if (a == System.Net.HttpStatusCode.OK)
            {
                InputMethodManager imm = (InputMethodManager)GetSystemService(Context.InputMethodService);
                imm.HideSoftInputFromWindow(etPassword.WindowToken, 0);
                btLogin.Enabled = false;
                etLogin.Enabled = false;
                etPassword.Enabled = false;
                boxSaveLoginData.Enabled = false;
                btUnLogin.Enabled = true;
                if (IsNeedLoginSave)
                {
                    var json = JsonConvert.SerializeObject(APIAuth.ActualUser);
                    var filePath = ApplicationPath + @"\" + fileName;
                    if (File.Exists(filePath))
                        File.Delete(filePath);
                    using (FileStream fs = File.Create(filePath))
                    {
                        Byte[] info = new UTF8Encoding(true).GetBytes(json);
                        // Add some information to the file.
                        fs.Write(info, 0, info.Length);
                    }
                }
            }
        }

        private void BtOpenCamera_Click(object sender, System.EventArgs e)
        {
            GetCameraEvents();
        }

        private async void GetCameraEvents()
        {
            try
            {
                HttpClient client = new HttpClient();
                // client.DefaultRequestHeaders.Authorization = 
                HttpRequestMessage request = new HttpRequestMessage();
                request.RequestUri = new Uri("https://a100.technovik.ru:1000/api/vik/GetCameraEvents?userName=" + APIAuth.ActualUser.login);
                request.Method = HttpMethod.Get;
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", APIAuth.UserToken);
                // request.Headers.Add("Content-Type", "application/json");
                HttpResponseMessage response = await client.SendAsync(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var resp = await response.Content.ReadAsStringAsync();
                    var map = JObject.Parse(resp);
                    Requests = map;
                    APIAuth.HasCameraRequest = true;

                    Intent intent = new Intent(MediaStore.ActionImageCapture);
                    StartActivityForResult(intent, 0);
                    // A100Toaster.Show(context, map["CameraEventId"].ToString());
                    // SendDeleteReq();

                }
                else
                {
                    A100Toaster.Show(Application, "Жду запроса на съемку...");
                    APIAuth.HasCameraRequest = false;
                    btSaveToGallery.Enabled = false;
                    btSendPhotoToServer.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                A100Toaster.Show(Application, "Ошибка");
            }
        }

        public async void SendDeleteReq()
        {
            try
            {
                HttpClient client = new HttpClient();
                // client.DefaultRequestHeaders.Authorization = 
                HttpRequestMessage request = new HttpRequestMessage();
                request.RequestUri = new Uri("https://a100.technovik.ru:1000/api/vik/GetCameraEvents?userName=" + APIAuth.ActualUser.login);
                request.Method = HttpMethod.Delete;
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", APIAuth.UserToken);
                // request.Headers.Add("Content-Type", "application/json");
                HttpResponseMessage response = await client.SendAsync(request);
                APIAuth.HasCameraRequest = false;

                // var resp = await response.Content.ReadAsStringAsync();
                // var map = JObject.Parse(resp);
            }
            catch
            {
                A100Toaster.Show(ApplicationContext, "Нет связи с сервером, перезайдите в приложение");
            }
        }


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        public bool OnNavigationItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.navigation_home:

                    // Ненужные компоненты
                    SetCameraVisibility(ViewStates.Gone);
                    SetGalleryVisibility(ViewStates.Gone);

                    // Компоненты страницы входа
                    SetLoginVisibility(ViewStates.Visible);

                    // проверка на актуальность токена
                    if (!APIAuth.LoggedIn)
                    {
                        btLogin.Enabled = true;
                        etLogin.Enabled = true;
                        etPassword.Enabled = true;

                        btOpenCamera.Enabled = false;
                        btSendPhotoToServer.Enabled = false;
                        btSaveToGallery.Enabled = false;
                        btUnLogin.Enabled = false;
                        // A100Toaster.Show(ApplicationContext, "Вы не вошли в A100! Войдите в свой аккаунт!");
                        // Toast.MakeText(ApplicationContext, "Добро подаловать, " + user.login + "!", ToastLength.Long).Show();
                    }
                    else
                    {
                        btUnLogin.Enabled = true;
                    }


                    return true;
                case Resource.Id.navigation_dashboard:

                    // Ненужные компоненты
                    SetLoginVisibility(ViewStates.Gone);
                    SetGalleryVisibility(ViewStates.Gone);

                    // Компоненты страницы камеры
                    SetCameraVisibility(ViewStates.Visible);

                    // Секция проверок
                    if (!APIAuth.LoggedIn)
                    {
                        btOpenCamera.Enabled = false;
                        btSendPhotoToServer.Enabled = false;
                        A100Toaster.Show(ApplicationContext, "Вы не вошли в A100! Войдите в свой аккаунт!");
                        // Toast.MakeText(ApplicationContext, "Добро подаловать, " + user.login + "!", ToastLength.Long).Show();
                    }
                    else
                    {
                        // TODO: сохранить в галерею
                        // TODO: загрузить немедленно
                        if (BitmapData == null)
                        {
                            btSendPhotoToServer.Enabled = false;
                            btSaveToGallery.Enabled = false;
                        }
                        else
                        {
                            btSendPhotoToServer.Enabled = true;
                            btSaveToGallery.Enabled = true;
                        }
                        btOpenCamera.Enabled = true;

                        // Если юзер вошел с первой вкладки, то все блокируем управление входом
                    }
                    return true;
                case Resource.Id.navigation_notifications:
                    SetLoginVisibility(ViewStates.Gone);
                    SetCameraVisibility(ViewStates.Gone);

                    SetGalleryVisibility(ViewStates.Visible);
                    return true;
            }
            return false;
        }
    }
}

