﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace A100_PocketCam.Models
{
    class CameraEvents
    {

        public int CameraEventId { get; set; }

        public string UserName { get; set; }

        public int vikID { get; set; }

        public byte[] ImageByte { get; set; }

        public int? Done { get; set; }
    }
}