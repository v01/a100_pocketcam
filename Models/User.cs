﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace A100_PocketCam.Models
{
    class User
    {
        public string login { get; set; }
        public string password { get; set; }

        public User(string login, string password)
        {
            this.login = login;
            this.password = password;
        }
    }
}