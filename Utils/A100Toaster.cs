﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace A100_PocketCam.Utils
{
    static class A100Toaster
    {
        public static void Show(Context context, string message)
        {
            Toast.MakeText(context, message, ToastLength.Long).Show();
        }
    }
}