package mono.android.app;

public class XamarinAndroidEnvironmentVariables
{
	// Variables are specified the in "name", "value" pairs
	public static final String[] Variables = new String[] {
		"MONO_DEBUG", "gen-compact-seq-points",
		"XAMARIN_BUILD_ID", "d9d2d3a5-a765-41df-a767-d09a45566309",
		"XA_HTTP_CLIENT_HANDLER_TYPE", "Xamarin.Android.Net.AndroidClientHandler",
		"XA_TLS_PROVIDER", "btls",
		"MONO_GC_PARAMS", "major=marksweep-conc",

	};
}
